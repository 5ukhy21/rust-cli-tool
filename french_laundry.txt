Thomas Keller’s legendary destination still doesn’t miss a beat. 
In fact this may be known as the greatest cooking space in America, as every aspect is carefully determined—from the counter height to the flowing lines of the ceiling. 
Chef Keller continues to pair classic French techniques with wildly fresh ingredients in a setting that is a perfect storm of restaurant greatness—we should all be so lucky to score a reservation here.
Dinner may bring signature oysters with white sturgeon caviar in a warm sabayon studded with tapioca pearls, or Pacific shima aji tartare composed with puffed grains and a vibrant tomato and jalapeño "chiffon". 
Wolf Ranch quail is then presented with white asparagus and dark cherries, while "Coffee and Donuts" is a perfect finish.