# Rust CLI Tool

## Demo
![](images/IDS_721_-_Rust_CLI_Tool.mp4){ width="800" height="600" style="display:block; margin-left:auto; margin-right:auto"}

## Project Overview
Throughout this semester, we have experimented with Rust and through this experimentation, we have been exposed to how powerful the language and deployments can actually be. One of the key ways of understanding and utilizing the power of this language is through a Command-Line Tool. For this project, I created a text search tool that can intake a word/string and find the lines within the provided where the word/string appears. To ensure that my command-line tool analyzed the strings properly, I created tests that would take into account the structure and capitalization of the given string compared to the text. In the case of this project, I utilized a text file that was composed of the Michelin Guide's description of the famous restaurant, The French Laundry.

## Screenshots
There are 3 key components to this CLI: 1) The main.rs file that conducts the most important part of the application - providing the result, 2) The lib.rs file that acts as a helper and support file, in which it holds all of the supporting functions, and 3) the accompanying tests.

### Provided Text/Prompt
![Text](images/Rust CLI Tool_Example Prompt.png)

### Main.rs
![main](images/Rust CLI Tool_Main rs.png)

### Lib.rs
![lib](images/Rust CLI Tool_Lib rs.png)

### Tests
![Tests](images/Rust CLI Tool_Tests.png)

### Test Results
![Test Results](images/Rust_CLI_Tool_Tests_Passed.png)

### Results
![Results](images/Rust CLI Tool_Results.png)